<?php

/** @noinspection DuplicatedCode */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Nette\Database\Explorer;
use Nette\Http\IRequest;
use Tracy\Debugger;

/**
 * Class DBCommunicator
 * @package MasterApp\DBNetworking
 */
class DBCommunicator extends BaseDBNetworking {

    public IRequest $request;

    public bool $debugModeEnabled;

    public bool $freshUpdate = false;

    private Explorer $context;

    private DBLogger $logger;

    private Storage $cache;

    private DBConstructor $constructor;

    /**
     * Communicator constructor.
     * @param Explorer $context
     * @param DBLogger $logger
     * @param Storage $cache
     * @param DBConstructor $constructor
     */
    public function __construct(Explorer $context, DBLogger $logger, Storage $cache, DBConstructor $constructor) {

        $this->context = $context;
        $this->logger = $logger;
        $this->cache = $cache;
        $this->constructor = $constructor;
    }

    public function setDebugModeEnabled(bool $debugModeEnabled): void { $this->debugModeEnabled = $debugModeEnabled; }

    /**
     * @param string $table
     * @param string $outputClass
     * @param int $page
     * @param int $size
     * @param string|null $order
     * @param int $invalidateTime
     * @param array $invalidateTags
     * @param array $cacheTags
     * @param array $filter
     * @return array
     * @throws DBLoggerException
     * @throws DBNetworkingCommunicatorDatabaseException
     * @throws DBNetworkingCommunicatorException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    #[ArrayShape(['count' => 'number', 'collection' => 'array'])]
    public function getAllEntities(string $table, string $outputClass, int $page, int $size, string $order = null, int $invalidateTime = 1,
       array $invalidateTags = [], array $cacheTags = [], array $filter = []): array {

        // Logger
        $debugObject = new DBDebugObject();
        $debugObject->callerFunction = __FUNCTION__;
        $debugObject->class = "Array<${outputClass}>";
        $debugObject->inputArguments = func_get_args();
        $debugObject->table = $table;
        $debugObject->page = $page;
        $debugObject->size = $size;
        $debugObject->invalidateTime = $invalidateTime;
        $debugObject->cacheTagsToRemove = $invalidateTags;
        $debugObject->cacheTags = $cacheTags;

        // Cache
        if ($this->debugModeEnabled) { $invalidateTime = 1; }
        try { $encodedFilter = json_encode($filter, JSON_THROW_ON_ERROR); } catch (JsonException $e) {
            throw new DBNetworkingCommunicatorException($debugObject, "Can't encode filter", $e);
        }
        $callerFunction = debug_backtrace()[1]['function'];
        $cacheKey = __DIR__ . "${callerFunction}|${table}|${encodedFilter}|${page}|${size}";
        $profilerKey = "GETALL-${table}";
        $debugObject->cachedKey = $cacheKey;

        // Recover from cache
        if ($invalidateTime !== 1 && ! $this->freshUpdate) {
            $value = $this->cache->read(hash('sha512', $cacheKey));
            if ($value !== null) {
                $debugObject->cached = true;
                $debugObject->result = $value;
                if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
                return unserialize($value, ['allowed_classes' => true]);
            }
        }

        // Get from database
        $result = null;
        $count = null;
        try {
            $result = $this->context->table($table)->where($filter)->page($page + 1, $size);
            if ($order !== null) { $result->order($order); }
            $result = $result->fetchAll();
            $count = $this->context->table($table)->where($filter)->count('*');
        } catch (Exception $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorDatabaseException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Iterator
        $outputCollection = [];
        foreach ($result as $item) {

            // Init entity
            $entity = new $outputClass();
            $debugObject->resultEntity = $item;
            try { $this->constructor->initializeEntity($item, $entity); }
            catch (DBNetworkingConstructorParameterNotFoundInEntityException
            | DBBaseEmptyParameterDocCommentException
            | DBNetworkingConstructorParameterWrongDataException
            | DBBaseWrongParameterDocCommentException $e) {
                $this->logger->generateDatabaseLog($debugObject, $this->request, false);
                if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
                throw new DBNetworkingCommunicatorException($debugObject, $e->getMessage(), $e->getPrevious());
            }

            $outputCollection[] = $entity;
        }

        // Write cache
        $outputResult = ['count' => $count, 'collection' => $outputCollection];
        $encodedResult = serialize($outputResult);
        $this->cache->write(hash('sha512', $cacheKey), $encodedResult,
        [Cache::EXPIRE => $invalidateTime, Cache::TAGS => array_merge([$callerFunction, $table], $cacheTags)]);
        if (! empty($invalidateTags)) { $this->cache->clean([Cache::TAGS => $invalidateTags]); }

        // Output
        $debugObject->result = $encodedResult;
        $this->logger->generateDatabaseLog($debugObject, $this->request);
        if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
        return $outputResult;
    }

    /**
     * @param string $table
     * @param array $filter
     * @param string $outputClass
     * @param int $invalidateTime
     * @param array $invalidateTags
     * @param array $cacheTags
     * @return ADBEntity
     * @throws DBLoggerException
     * @throws DBNetworkingCommunicatorDatabaseException
     * @throws DBNetworkingCommunicatorException
     * @throws DBNetworkingCommunicatorNotFoundException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function getEntityByFilter(string $table, array $filter, string $outputClass, int $invalidateTime = 1, array $invalidateTags = [], array $cacheTags = []): ADBEntity {

        // Logger
        $debugObject = new DBDebugObject();
        $debugObject->callerFunction = __FUNCTION__;
        $debugObject->class = $outputClass;
        $debugObject->inputArguments = func_get_args();
        $debugObject->table = $table;
        $debugObject->filter = $filter;
        $debugObject->invalidateTime = $invalidateTime;
        $debugObject->cacheTagsToRemove = $invalidateTags;
        $debugObject->cacheTags = $cacheTags;

        // Cache
        if ($this->debugModeEnabled) { $invalidateTime = 1; }
        $callerFunction = debug_backtrace()[1]['function'];
        try { $encodedFilter = json_encode($filter, JSON_THROW_ON_ERROR); } catch (JsonException $e) {
            throw new DBNetworkingCommunicatorException($debugObject, "Can't encode filter", $e);
        }
        $cacheKey = __DIR__ . "${callerFunction}|${table}|${encodedFilter}|${outputClass}";
        $profilerKey = "GETONE-${table}-${encodedFilter}";
        $debugObject->cachedKey = $cacheKey;

        // Recover from cache
        if ($invalidateTime !== 1 && ! $this->freshUpdate) {
            $value = $this->cache->read(hash('sha512', $cacheKey));
            if ($value !== null) {
                $debugObject->cached = true;
                $debugObject->result = $value;
                if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
                return unserialize($value, ['allowed_classes' => true]);
            }
        }

        // Call DB
        $result = null;
        try { $result = $this->context->table($table)->where($filter)->fetch(); } catch (Exception $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorDatabaseException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Not found
        if ($result === null) {
            throw new DBNetworkingCommunicatorNotFoundException($debugObject, "Not possible to find entity ${outputClass} with filter");
        }

        // Initialize entity
        $entity = new $outputClass();
        $debugObject->resultEntity = $result;
        try { $this->constructor->initializeEntity($result, $entity); }
        catch (DBNetworkingConstructorParameterNotFoundInEntityException
        | DBBaseEmptyParameterDocCommentException
        | DBNetworkingConstructorParameterWrongDataException
        | DBBaseWrongParameterDocCommentException $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorException($debugObject, 'Problem while initializing entity', $e->getPrevious());
        }

        // Write cache
        $encodedResult = serialize($entity);
        $this->cache->write(hash('sha512', $cacheKey), $encodedResult,
            [Cache::EXPIRE => $invalidateTime, Cache::TAGS => array_merge([$callerFunction, $table], $cacheTags)]);
        if (! empty($invalidateTags)) { $this->cache->clean([Cache::TAGS => $invalidateTags]); }

        // Output
        $debugObject->result = $encodedResult;
        $this->logger->generateDatabaseLog($debugObject, $this->request);
        if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
        return $entity;
    }

    /**
     * @param string $table
     * @param ADBEntity $inputEntity
     * @param string|null $outputClass
     * @param array $invalidateTags
     * @return ADBEntity
     * @throws DBLoggerException
     * @throws DBNetworkingCommunicatorDatabaseException
     * @throws DBNetworkingCommunicatorException
     * @throws DBNetworkingCommunicatorNotFoundException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function createEntity(string $table, ADBEntity $inputEntity, ?string $outputClass = null, array $invalidateTags = []): ADBEntity {

        // Logger
        if ($outputClass === null) { $outputClass = get_class($inputEntity); }
        $debugObject = new DBDebugObject();
        $debugObject->callerFunction = __FUNCTION__;
        $debugObject->class = $outputClass;
        $debugObject->inputArguments = func_get_args();
        $debugObject->table = $table;
        $profilerKey = "INSERT-${table}-${outputClass}";

        // Put to database
        $result = null;
        try { $result = $this->context->table($table)->insert((array) $inputEntity); } catch (Exception $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorDatabaseException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Not found
        if (! is_object($result)) {
            throw new DBNetworkingCommunicatorNotFoundException($debugObject, "Not possible to insert entity ${outputClass}");
        }

        // Initialize entity
        $entity = new $outputClass();
        $debugObject->resultEntity = $result;
        try { $this->constructor->initializeEntity($result, $entity); }
        catch (DBNetworkingConstructorParameterNotFoundInEntityException
        | DBBaseEmptyParameterDocCommentException
        | DBNetworkingConstructorParameterWrongDataException
        | DBBaseWrongParameterDocCommentException $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Cache
        $this->cache->clean([Cache::TAGS => $invalidateTags]);

        // Output
        $encodedResult = serialize($entity);
        $debugObject->result = $encodedResult;
        $this->logger->generateDatabaseLog($debugObject, $this->request);
        if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
        return $entity;
    }

    /**
     * @param string $table
     * @param ADBEntity $inputEntity
     * @param array $invalidateTags
     * @throws DBLoggerException
     * @throws DBNetworkingCommunicatorDatabaseException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function updateEntity(string $table, ADBEntity $inputEntity, array $invalidateTags = []): void {

        // Logger
        $debugObject = new DBDebugObject();
        $debugObject->callerFunction = __FUNCTION__;
        $debugObject->class = get_class($inputEntity);
        $debugObject->inputArguments = func_get_args();
        $debugObject->table = $table;
        $profilerKey = "UPDATE-${table}-{$debugObject->class}";

        // Put to database
        $result = null;
        try {
            $id = $inputEntity->id;
            $this->context->table($table)->where('id', $id)->update((array) $inputEntity);
        } catch (Exception $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorDatabaseException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Cache
        $this->cache->clean([Cache::TAGS => $invalidateTags]);
    }

    /**
     * @param string $table
     * @param ADBEntity $inputEntity
     * @param array $invalidateTags
     * @throws DBLoggerException
     * @throws DBNetworkingCommunicatorDatabaseException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function deleteEntity(string $table, ADBEntity $inputEntity, array $invalidateTags = []): void {

        // Logger
        $debugObject = new DBDebugObject();
        $debugObject->callerFunction = __FUNCTION__;
        $debugObject->class = get_class($inputEntity);
        $debugObject->inputArguments = func_get_args();
        $debugObject->table = $table;
        $profilerKey = "DELETE-${table}-{$debugObject->class}";

        // Put to database
        $result = null;
        try {
            $id = $inputEntity->id;
            $this->context->table($table)->where('id', $id)->delete();
        } catch (Exception $e) {
            $this->logger->generateDatabaseLog($debugObject, $this->request, false);
            if ($this->debugModeEnabled) { Debugger::barDump($debugObject, $profilerKey); }
            throw new DBNetworkingCommunicatorDatabaseException($debugObject, $e->getMessage(), $e->getPrevious());
        }

        // Cache
        $this->cache->clean([Cache::TAGS => $invalidateTags]);
    }
}