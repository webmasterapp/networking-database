<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;

class DBNetworkingCommunicatorNotFoundException extends DBDebugObjectException {}