<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use Nette\Database\Table\ActiveRow;

/**
 * Class DBDebugObject
 * @package MasterApp\Logger
 */
class DBDebugObject {

    public string $callerFunction;

    public string $table;

    public ?int $page = null;

    public ?int $size = null;

    public string $class;

    public array $inputArguments;

    public ?ActiveRow $resultEntity = null;

    public bool $cached = false;

    public ?string $cachedKey = null;

    public array $cacheTags = [];

    public array $cacheTagsToRemove = [];

    public ?int $invalidateTime = null;

    public ?string $result = null;

    public ?array $filter = null;
}