<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use DateTime;
use Nette\Database\Table\ActiveRow;

/**
 * Class DBConstructor
 * @package MasterApp\DBNetworking
 */
class DBConstructor extends BaseDBNetworking {

    /**
     * @param ActiveRow $activeRow
     * @param ADBEntity $entity
     * @throws DBBaseEmptyParameterDocCommentException
     * @throws DBBaseWrongParameterDocCommentException
     * @throws DBNetworkingConstructorParameterNotFoundInEntityException
     * @throws DBNetworkingConstructorParameterWrongDataException
     */
    public function initializeEntity(ActiveRow $activeRow, ADBEntity $entity): void {

        $entityClass = get_class($entity);
        $constructionObjectParameters = self::getEntityParameters($entityClass, $this->entitiesNamespace);
        foreach ($constructionObjectParameters as $parameter) {

            $parameterName = $parameter->name;
            $message = "Parameter ${parameterName} missing in class ${entityClass} but found in result";
            if (! property_exists($entity, $parameterName)) { throw new DBNetworkingConstructorParameterNotFoundInEntityException($message); }

            if ($parameter->type === 'bool') {
                $entity->{$parameterName} = match ($activeRow->{$parameterName}) {
                    1 => true,
                    0 => false,
                    default => throw new DBNetworkingConstructorParameterWrongDataException("Parameter ${parameterName} marked as bool but has wrong data!")
                };
                continue;
            }

            if (enum_exists($parameter->type)) {
                $entity->{$parameterName} = $parameter->type::from($activeRow->{$parameterName});
                continue;
            }

            if ($parameter->type instanceof DateTime) {
                $entity->{$parameterName} = DateTime::createFromFormat('Y-m-d H:i:s', $activeRow->{$parameterName});
                continue;
            }

            $entity->{$parameterName} = $activeRow->{$parameterName};
        }
    }
}

