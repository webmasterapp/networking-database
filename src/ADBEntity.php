<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;

/**
 * Class ADBEntity
 * @package MasterApp\DBNetworking
 */
abstract class ADBEntity {

    public int $id;
}