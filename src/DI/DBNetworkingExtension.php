<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking\DI;
use MasterApp\DBNetworking\DBCommunicator;
use MasterApp\DBNetworking\DBConstructor;
use MasterApp\DBNetworking\DBLogger;
use MasterApp\DBNetworking\DBTestCreator;
use Nette\DI\CompilerExtension;

/**
 * Class DBNetworkingExtension
 * @package MasterApp\DBNetworking\DI
 */
class DBNetworkingExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('dbNetworkingConstructor'))
            ->addSetup('setEntitiesNamespace', [$config['entitiesNamespace']])
            ->setType(DBConstructor::class);

        $builder->addDefinition($this->prefix('dbNetworkingTestCreator'))
            ->addSetup('setEntitiesNamespace', [$config['entitiesNamespace']])
            ->setType(DBTestCreator::class);

        $builder->addDefinition($this->prefix('dbNetworkingLogger'))
            ->setType(DBLogger::class)
            ->addSetup('setLogPath', [$config['logPath']])
            ->addSetup('setVersion', [$config['version'] ?? 'without version'])
            ->addSetup('setDebugModeEnabled', [$config['debugModeEnabled']]);

        $builder->addDefinition($this->prefix('dbNetworkingCommunicator'))
            ->setType(DBCommunicator::class)
            ->addSetup('setDebugModeEnabled', [$config['debugModeEnabled']]);
    }
}
