<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use DateTime;
use Exception;
use JsonException;
use MasterApp\Mattermost\Mattermost;
use MasterApp\Mattermost\MattermostException;
use Nette\Http\IRequest;
use RuntimeException;
use stdClass;

/**
 * Class DBLogger
 * @package MasterApp\DBNetworking
 */
class DBLogger {

    private const Line = '----------------------------------------------------------------------------------------------------';

    public string $logPath;

    public string $version;

    public bool $debugModeEnabled;

    private Mattermost $mattermost;

    /** @param Mattermost $mattermost */
    public function __construct(Mattermost $mattermost) {
        $this->mattermost = $mattermost; }

    public function setLogPath(string $logPath): void {
        $this->logPath = $logPath; }

    public function setVersion(string $version): void {
        $this->version = $version; }

    /** @param bool $debugModeEnabled */
    public function setDebugModeEnabled(bool $debugModeEnabled): void {
        $this->debugModeEnabled = $debugModeEnabled; }

    /**
     * @param DBDebugObject $debugObject
     * @param IRequest $request
     * @param bool $success
     * @throws DBLoggerException
     */
    public function generateDatabaseLog(DBDebugObject $debugObject, IRequest $request, bool $success = true): void {

        if ($this->checkIsMattermost($request)) { return; }
        $logPathWithDir = $this->setUpLogDir();
        $notify = ! $this->debugModeEnabled ? '<!channel>' : '';
        $debugMode = $this->debugModeEnabled ? 'debugMode' : 'prodMode';

        try {
            $debug = $this->getRequestDebugObject($request);
            $encodedResult = $debugObject->resultEntity !== null ? json_encode($debugObject->resultEntity, JSON_THROW_ON_ERROR) : null;
            $encodedInputParameters = json_encode($debugObject->inputArguments, JSON_THROW_ON_ERROR);
            $cacheKey = $debugObject->cachedKey === null ? null : str_replace('|', '@', $debugObject->cachedKey);
            $cacheRemove = json_encode($debugObject->cacheTagsToRemove, JSON_THROW_ON_ERROR);
            $cacheTags = json_encode($debugObject->cacheTags, JSON_THROW_ON_ERROR);
            $encodedFilter = json_encode($debugObject->filter, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new DBLoggerException("Can't encode logging data", $exception->getCode(), $exception);
        }

        $message = "** :apierror: Database error thrown **\n";
        $message .= "Please review the error output. ${notify}\n";
        $message .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n\n";
        $message .= "| Type          | Info                         |
                     | :------------ |:----------------------------:|
                     | Version/ENV   | {$this->version} / ${debugMode}  | 
                     | Table         | {$debugObject->table}          |
                     | Function      | {$debugObject->callerFunction} |
                     | Entity        | {$debugObject->class}          |
                     | CacheKey      | `${cacheKey}`                  |
                     | CacheTime     | {$debugObject->invalidateTime} |
                     | CacheRemove   | `${cacheRemove}`               |
                     | CacheTags     | `${cacheTags}`                 |
                     | URL           | {$debug->url}                  |
                     | Client IP     | {$debug->remote}               |
                     | Body          | `{$debug->body}`               |
                     | Headers       | `{$debug->headers}`            |
                     | Parameters    | `${encodedInputParameters}`    |
                     | Filter        | `${encodedFilter}`             |
                     | DBResult      | `${encodedResult}`             |
                     | Result        | `{$debugObject->result}`       |\n";

        if (! $success) {
            try { $this->mattermost->sendMessage($message); } catch (MattermostException $e) {
                throw new DBLoggerException('Can not send mattermost message', $e->getCode(), $e);
            }
        }

        // Log locally
        try {
            $outputData = "Database log\n";
            $outputData .= 'Time of log entry: ' . date('d/m/Y H:i:s') . "\n";
            $outputData .= json_encode($debugObject, JSON_THROW_ON_ERROR);
            $outputData .= "\n\n" . self::Line . "\n";
        } catch (JsonException $exception) {
            throw new DBLoggerException('Can not encode output file debug object', $exception->getCode(), $exception);
        }

        $fileName = $success ? (new DateTime())->format('Y-m-d') . '-OK.txt' : (new DateTime())->format('Y-m-d') . '-ERR.txt';
        file_put_contents($logPathWithDir . $fileName, $outputData, FILE_APPEND);
    }

    /**
     * @return string
     */
    private function setUpLogDir(): string {
        $logPathWithDir = $this->logPath . '/database/';
        if (! file_exists($logPathWithDir) && ! mkdir($logPathWithDir, 0777, true) && ! is_dir($logPathWithDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $logPathWithDir));
        }
        return $logPathWithDir;
    }

    /**
     * @param IRequest $request
     * @return bool
     */
    private function checkIsMattermost(IRequest $request): bool {

        $agent = $request->getHeader('user-agent');
        return empty($agent) || str_contains($agent, 'mattermost');
    }

    /**
     * @param IRequest $request
     * @return stdClass
     * @throws JsonException
     */
    private function getRequestDebugObject(IRequest $request): stdClass {

        $debug = new stdClass();
        $debug->url = $request->getUrl();
        $debug->remote = $request->getRemoteAddress();
        $debug->headers = json_encode($request->getHeaders(), JSON_THROW_ON_ERROR);
        $debug->body = json_encode($request->getRawBody(), JSON_THROW_ON_ERROR);
        return $debug;
    }
}

/**
 * Class DBLoggerException
 * @package MasterApp\DBNetworking
 */
class DBLoggerException extends Exception {}
