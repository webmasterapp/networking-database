<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;

class DBNetworkingCommunicatorException extends DBDebugObjectException {}