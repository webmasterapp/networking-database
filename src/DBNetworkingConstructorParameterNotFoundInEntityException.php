<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;

use Exception;

class DBNetworkingConstructorParameterNotFoundInEntityException extends Exception {}