<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use Exception;
use JetBrains\PhpStorm\Pure;
use Throwable;

/**
 * Class DBDebugObjectException
 * @package MasterApp\Logger
 */
abstract class DBDebugObjectException extends Exception {

    public ?DBDebugObject $debugObject;

    /**
     * DBDebugObjectException constructor.
     * @param DBDebugObject|null $debugObject
     * @param string $message
     * @param Throwable|null $previous
     */
    #[Pure] public function __construct(DBDebugObject $debugObject = null, $message = '', Throwable $previous = null) {
        parent::__construct($message, 0, $previous);
        $this->debugObject = $debugObject;
    }
}