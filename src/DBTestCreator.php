<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use Exception;
use ReflectionException;

/**
 * Class DBTestCreator
 * @package MasterApp\DBNetworking
 */
class DBTestCreator extends BaseDBNetworking {

    /**
     * @param ADBEntity $entity
     * @param string $location
     * @throws ReflectionException
     * @throws Exception
     */
    public function processAssertions(ADBEntity $entity, string $location = 'result'): void {

        $referenceObjectClass = get_class($entity);
        $referenceObjectParameters = self::getEntityParameters($referenceObjectClass, $this->entitiesNamespace);
        foreach ($referenceObjectParameters as $parameter) {

            // Null
            $name = $parameter->name;
            $newLocation = $location . '->' . $name;
            if ($entity->{$name} === null) { continue; }

            // Array
            if ($parameter->type === 'array' && ! empty($entity->{$name})) {
                $this->processAssertions($entity->{$name}[0], $location . '->' . $name . '[0]');
            }

            // Entity
            if (is_subclass_of($parameter->type, ADBEntity::class)) {
                $this->processAssertions($entity->{$name}, $newLocation);
            }

            // Other
            $this->printAssertion($newLocation, $parameter->type);
        }
    }

    /**
     * @param array $list
     * @param string $location
     * @throws ReflectionException
     * @throws Exception
     */
    public function processAssertionsFromCollection(array $list, string $location = 'result'): void {

        if (empty($list)) { return; }
        $this->processAssertions($list[0], $location . '[0]');
        $this->printAssertion($location, 'array');
    }

    /**
     * @param string $location
     * @param string $type
     * @throws Exception
     */
    private function printAssertion(string $location, string $type): void {

        $randomIdentifier = self::randHash();
        if ($type === 'double') { $type = 'float'; }
        echo "Assert::type('${type}', \$${location}, '${randomIdentifier}');\n";
    }

    /**
     * @return string
     * @throws Exception
     */
    private static function randHash(): string {
        return substr(md5(random_bytes(20)),-(32));
    }
}