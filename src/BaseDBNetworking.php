<?php

declare(strict_types=1);

namespace MasterApp\DBNetworking;
use Exception;
use ReflectionClass;
use ReflectionException;
use stdClass;

/**
 * Class BaseDBNetworking
 * @package MasterApp\DBNetworking
 */
abstract class BaseDBNetworking {

    public string $entitiesNamespace;

    public function setEntitiesNamespace(string $entitiesNamespace): void {
        $this->entitiesNamespace = $entitiesNamespace; }

    /**
     * @param string $class
     * @param string $entitiesNameSpace
     * @return array
     * @throws DBBaseEmptyParameterDocCommentException
     * @throws DBBaseWrongParameterDocCommentException
     */
    protected static function getEntityParameters(string $class, string $entitiesNameSpace): array {

        $parameters = array_keys(get_class_vars($class));
        try { $reflectionClass = new ReflectionClass($class); } catch (ReflectionException $e) {
            throw new DBBaseWrongParameterDocCommentException("Problem while parsing class reflection for class ${class}");
        }

        $outputParameters = [];
        foreach ($parameters as $parameter) {

            try {$property = $reflectionClass->getProperty($parameter); } catch (ReflectionException $e) {
                throw new DBBaseWrongParameterDocCommentException("Problem while parsing class reflection for class ${class} and parameter ${parameter}");
            }

            $comment = $property->getDocComment();
            if ($comment === false) { throw new DBBaseEmptyParameterDocCommentException("Parameter ${parameter} of class ${class} has empty doc"); }
            $re = '/\/[*]+\s@var\s([a-zA-Z0-9\\\\]+)(<[a-zA-Z0-9]+>)?(\|null)?/m';
            preg_match_all($re, $comment, $matches, PREG_SET_ORDER);
            if (! isset($matches[0][1])) { throw new DBBaseWrongParameterDocCommentException("Parameter ${parameter} of class ${class} has wrong doc: ${comment}"); }

            $isBaseType = $matches[0][1] === 'int' || $matches[0][1] === 'string' || $matches[0][1] === 'float' || $matches[0][1] === 'array' || $matches[0][1] === 'bool';
            $isDateTime = $matches[0][1] === 'DateTime';
            if ($isBaseType) { $propertyClass = $matches[0][1]; }
            else if ($isDateTime) { $propertyClass = '\\' . $matches[0][1]; }
            else { $propertyClass = $entitiesNameSpace . '\\' . $matches[0][1]; }
            $classToInit = isset($matches[0][2]) && $propertyClass === 'array' ? trim($matches[0][2], '<>') : null;

            $outputParameter = new stdClass();
            $outputParameter->name = $parameter;
            $outputParameter->type = $propertyClass;
            $outputParameter->class = $classToInit !== null ? $entitiesNameSpace . '\\' . $classToInit : null;
            $outputParameters[] = $outputParameter;
        }

        return $outputParameters;
    }
}

/**
 * Class DBBaseWrongParameterDocCommentException
 * @package MasterApp\DBNetworking
 */
class DBBaseWrongParameterDocCommentException extends Exception {}

/**
 * Class DBBaseEmptyParameterDocCommentException
 * @package MasterApp\DBNetworking
 */
class DBBaseEmptyParameterDocCommentException extends Exception {}